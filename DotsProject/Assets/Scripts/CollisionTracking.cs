using UnityEngine;

public class CollisionTracking : MonoBehaviour
{
    Rigidbody2D rigidbody;
    Vector2 lastFrameVelocity;

    public string hostileTag;
    public float collisionDamage;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();

        //applies starter collision
        //  rigidbody.AddForce(Vector2.one * 400);
    }

    private void Update()
    {
        //stores last frame's velocity to calculate against during collision
        lastFrameVelocity = rigidbody.velocity;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == hostileTag)
        {
            if (collision.gameObject.GetComponent<Health>() != null)
            {
                collision.gameObject.GetComponent<Health>().currentHealth -= collisionDamage;

            }

        }



        // CheckDeath();
        Bounce(collision);
    }

    void Bounce(Collision2D collision)
    {
        //reflects the direction of the rigibody's velocity by its previous trajectory and the object it collided with
        //and assigns the new velocity to the object
        var newForwardDirection = Vector3.Reflect(lastFrameVelocity, collision.contacts[0].normal);
        transform.up = newForwardDirection;
        print(newForwardDirection);

        rigidbody.AddForce(transform.forward * 10, ForceMode2D.Impulse);
    }




    //void CheckDeath()
    //{
    //    float deathNumber = Random.Range(0, 100);
    //    if (deathNumber > 50)
    //    {
    //        Destroy(gameObject);
    //    }
    //    print("Death number is " + deathNumber);
    //}
}
