using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerProjectile : MonoBehaviour
{
    public float projectileDamage;
    public float movementSpeed;
    public string tagToTarget;
    public float lifeTime;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("KillTimer");
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.up * Time.deltaTime * movementSpeed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == tagToTarget)
        {
            if (collision.gameObject.GetComponent<Health>() != null)
            {
                collision.gameObject.GetComponent<Health>().currentHealth -= projectileDamage;
                Destroy(gameObject);
            }
            print("Applying damage");
        }
    }

    IEnumerator KillTimer()
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }

}
