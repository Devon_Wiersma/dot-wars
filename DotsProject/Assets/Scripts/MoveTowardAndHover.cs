using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardAndHover : MonoBehaviour
{
    public GameObject projectile;
    GameObject targetObject;
    public string targetTag;
    public float maxMovementSpeed;
    public float movementAcceleration;
    public float rotationSpeed;
    public float hoverDistance;
    private float currentSpeed;
    private Rigidbody2D m_Rigidbody;

    public float fireRate;
    public float fireSpread;
    bool canFire = true;
    bool hasTurret;
    public TurretController[] _turrets;



    // Start is called before the first frame update
    void Start()
    {
        if (GetComponentInChildren<TurretController>() != null)
        {
            _turrets = GetComponentsInChildren<TurretController>();
        }

        m_Rigidbody = transform.GetComponent<Rigidbody2D>();
        currentSpeed = 0;

        transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360)));

        AcquireTarget();
    }

    // Update is called once per frame
    void Update()
    {
        if (targetObject != null)
        {
            RotateToTarget();
            MoveForward();
        }
        else
        {
            AcquireTarget();
        }

        Debug.DrawRay(transform.position, transform.up, Color.green);
    }

    public void MoveForward()
    {
        if (hoverDistance <= (targetObject.transform.position - transform.position).magnitude)
        {
            currentSpeed = Mathf.Clamp(currentSpeed + (movementAcceleration * Time.deltaTime), -maxMovementSpeed, maxMovementSpeed);
            m_Rigidbody.velocity = transform.up * currentSpeed;

        }
        else
        {
            m_Rigidbody.velocity = transform.up / currentSpeed;
            //if (canFire)
              //  StartCoroutine("Fire");
        }
    }

    public void RotateToTarget()
    {
        //get target angle and convert it to a float using ATAN
        // the 90 is an offset applied to compensate for the forward direction of the transform
        //once we have the angle we apply that rotation to the object and slerp it to the desired direction
        var dir = targetObject.transform.position - transform.position;
        dir.Normalize();
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        Quaternion targetRotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));

        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);


    }

    public void AcquireTarget()
    {
        targetObject = GameObject.FindGameObjectWithTag(targetTag);
    }
}
