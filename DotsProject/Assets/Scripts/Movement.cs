using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    GameObject targetObject;
    public string targetTag;
    public float maxMovementSpeed;
    public float movementAcceleration;
    public float rotationSpeed;
    [Range(0, 1)]
    public float directionalAccuracy;
    float variance;
    private float currentSpeed;
    private Rigidbody2D m_Rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        m_Rigidbody = transform.GetComponent<Rigidbody2D>();
        variance = Random.Range((float)0, 0.5f);
        currentSpeed = 0;

        transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360)));

        AcquireTarget();
    }

    // Update is called once per frame
    void Update()
    {
        if (targetObject != null)
        {
            RotateToTarget();
            MoveForward();
        }
        else
        {
            AcquireTarget();
        }

        Debug.DrawRay(transform.position, transform.up, Color.green);
    }

    public void MoveForward()
    {
        var headingDot = CheckHeading();
        if (headingDot > directionalAccuracy)
        {
            currentSpeed = Mathf.Clamp(currentSpeed + (movementAcceleration * Time.deltaTime), -maxMovementSpeed, maxMovementSpeed);
            m_Rigidbody.velocity = transform.up * currentSpeed;
        }
        else
        {
            m_Rigidbody.velocity = Vector3.Lerp(m_Rigidbody.velocity, Vector2.zero, movementAcceleration * Time.deltaTime);
        }

    }


    public void RotateToTarget()
    {
        //get target angle and convert it to a float using ATAN
        // the 90 is an offset applied to compensate for the forward direction of the transform
        //once we have the angle we apply that rotation to the object and slerp it to the desired direction
        var dir = targetObject.transform.position - transform.position;
        dir.Normalize();
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        Quaternion targetRotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));

        var dotHeading = CheckHeading();
        if (dotHeading < directionalAccuracy)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
        }
        else
        {

        }

    }

    public float CheckHeading()
    {
        var targetDir = (targetObject.transform.position - transform.position).normalized;
        var dot = (float)Vector2.Dot(targetDir, transform.up);
       // print("Dot for " + gameObject.name + " is " + dot);
        return dot;
    }

    public void AcquireTarget()
    {
        targetObject = GameObject.FindGameObjectWithTag(targetTag);
    }

}
