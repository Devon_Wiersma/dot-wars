using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour
{
    public GameObject projectilePrefab;

    public GameObject targetObject;
    public string targetTag;

    public float fireRate;
    public bool canFire;
    Coroutine fireRoutine;

    public bool autoLock;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (autoLock && targetObject != null)
        {
            transform.up = targetObject.transform.position - transform.position;
        }

        if (canFire)
        {
            if (fireRoutine == null)
            {
                fireRoutine = StartCoroutine("Fire");
            }
            else
            {

            }
        }


    }

    public IEnumerator Fire()
    {
        print("FIRING");
        yield return new WaitForSeconds(fireRate);

        var newProjectile = Instantiate(projectilePrefab);
        newProjectile.transform.rotation = transform.rotation;

        if (targetTag == "Enemy")
        {
            newProjectile.layer = LayerMask.NameToLayer("FriendlyProjectile");
        }
        else
        {
            newProjectile.layer = LayerMask.NameToLayer("EnemyProjectile");
        }

        newProjectile.transform.Rotate(0, 0, Random.Range(-fireRate, fireRate));
        newProjectile.transform.position = transform.position + (transform.forward * 15f);
        newProjectile.GetComponent<LazerProjectile>().tagToTarget = targetTag;

        fireRoutine = null;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (targetObject == null)
        {
            if (targetTag == collision.gameObject.tag)
            {
                targetObject = collision.gameObject;
                canFire = true;
            }

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (targetObject = collision.gameObject)
        {
            targetObject = null;
            canFire = false;
            StopAllCoroutines();
            fireRoutine = null;
        }
    }
}
